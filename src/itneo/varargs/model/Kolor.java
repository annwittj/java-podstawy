package itneo.varargs.model;

public enum Kolor {
    CZERWONY,
    CZARNY,
    NIEBIESKI;
}
